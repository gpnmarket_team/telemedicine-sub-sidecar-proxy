FROM artifactory.gazprom-neft.local/ci-docker-local/dev-base/nginx-118-rhel7-gpn

USER root

COPY ./addUserAndGroup.sh ./
RUN chmod +x ./addUserAndGroup.sh && ./addUserAndGroup.sh && rm -f ./addUserAndGroup.sh

COPY ./sub-stream.conf /usr/share/nginx/modules/
RUN chown -R 1001020000:1500 /opt/ /var/log/nginx

USER 1001020000

CMD $STI_SCRIPTS_PATH/run
